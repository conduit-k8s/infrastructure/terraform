variable "vpc_name" {
    type = string
    default = "Conduit-k8s-vpc"
}

variable "vpc_cidr" {
    type = string
    default = "10.0.0.0/16"
  
}

variable "vpc_azs" {
    type = list
    default = ["eu-central-1a", "eu-central-1b"]
}

variable "vpc_private_subnets" {
    type = list
    default = ["10.0.1.0/24", "10.0.2.0/24"]
  
}

variable "vpc_public_subnets" {
    type = list
    default = ["10.0.101.0/24", "10.0.102.0/24"]
  
}

variable "tags" {
    type    = map
    default = {
        Environment = "prod-k8s-conduit"
    }
}

variable "vpc_public_subnet_tags" {
    type    = map
    default = {
        "kubernetes.io/role/elb"     = "1"
        "kubernetes.io/cluster/conduit-k8s" = "owned"
    }
}

variable "vpc_private_subnet_tags" {
    type    = map
    default = {
        "kubernetes.io/role/internal-elb"     = "1"
        "kubernetes.io/cluster/conduit-k8s" = "owned"
    }
}